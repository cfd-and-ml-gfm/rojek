#!/bin/bash
sbatch --parsable --dependency=afterok:$1 <<EOT
#!/bin/bash

#SBATCH --job-name="pos-$3"
#SBATCH -o $2"/%x-%j.out"
#SBATCH --ntasks 1
#SBATCH --time 00-02:00:00

#cd $2
#python ../../reconsPar.py
reconstructPar -case $2
rm -rf processor*

EOT
