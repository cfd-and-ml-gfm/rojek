#!/usr/bin/env python
# coding: utf-8
import os
import sys
import shutil  
import subprocess as sp
from platform import python_version
from multiprocessing import Pool, cpu_count
import time

def callReconstruct(ts):
    #caseId = "{:.2f}".format(round(float(ts) / 100, 2))
    command = "reconstructPar -time %s" % (ts)
    print("Running %s ..." % (command))
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]

if __name__ == "__main__":
    print('Python', python_version())   

    p = Pool(cpu_count())
    p.map(callReconstruct, range(4202))
    p.close()
    p.join()