#!/bin/bash

mkdir $2

sbatch --parsable <<EOT
#!/bin/bash

#SBATCH --job-name="pre-$7"
#SBATCH -o $2"/%x-%j.out"
##SBATCH -e $2"/%x-%j.err"
#SBATCH --ntasks 1
#SBATCH --time 00-00:02:00

set -e	# Set exit-on-error

echo "JobID: $SLURM_JOB_ID"
echo "Base: $1 - Destination: $2"

cp -r $1/0 $2
cp -r $1/1 $2
cp -r $1/constant $2
cp -r $1/system $2
cp $1/parallel $2

printf "inlet\t$3;\nrecirc\t$4;\nu1\t$5;\nu2\t$6;\n" > $2/params
cat $2/params

decomposePar -case $2 -force

EOT