#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
import pathlib  
import shutil  
import glob
from platform import python_version
from pathlib import Path

def get_params_from_file():
    command = "head -n 32 cases_list.txt | tail -n1"
    print(command)
    child = sp.Popen(command, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    streamdata = child.communicate()[0]
    params = streamdata.decode().split()

    return float(params[4]), float(params[3])
    
if __name__ == "__main__":
    srcpath = "case-base"

    if False:
        dstpath = "casePredicted"
        predictedCase = "pred.npz"
    else:
        dstpath = "caseTest"
        predictedCase = "test.npz"

    if os.path.exists(dstpath) and os.path.isdir(dstpath):
        print("Removing directory %s ..." % (dstpath))
        shutil.rmtree(dstpath)
      
    print("Copying %s in %s ..." % (srcpath, dstpath))
    shutil.copytree(srcpath, dstpath)
    
    command = "touch %s/%s.foam" % (dstpath, dstpath)
    print(command)
    child = sp.Popen(command.split(), stdout=sp.PIPE)
    streamdata = child.communicate()[0]
    
    filename = "CASES/case1305/1001/U"
    header = ""
    footer = ""   
    headerlines = 21
    with open(filename, 'r') as reader:
        line = reader.readline()
        cntline = 0
        cntarray = 0
        cntfooter = 0
        while line:
            if cntline <= headerlines:
                header = header + line
                if cntline == headerlines:
                    ncells = int(line.strip())
                    reader.readline()
                    cntline += 1
            elif cntline > (headerlines + ncells +1):
                if cntfooter == 12:
                    line = "\tvolumetricFlowRate\tconstant\t0;\n";
                elif cntfooter == 19:
                    line = "\tvolumetricFlowRate\tconstant\t0;\n";
                footer = footer + line
                cntfooter += 1 
            line = reader.readline()
            cntline += 1
    
    ds = np.load(predictedCase)
    ds = ds.f.data
    print(ds.shape)

    #for ts in range(ds.shape[0]):
    for ts in range(100):
        values = "(\n"
        for vels in range(ds.shape[1]):
            cell = "(%f %f %f)\n" % (ds[ts][vels][0], ds[ts][vels][1], ds[ts][vels][2])
            values = values + cell

        Path("%s/%d" % (dstpath, ts + 3)).mkdir(parents=True, exist_ok=True)
        filename = "%s/%d/U" % (dstpath, ts + 3)
        print("Writing file %s ..." % filename)
        with open(filename, 'w') as writer:     
            writer.write(header)
            writer.write(values)
            writer.write(footer)
    
