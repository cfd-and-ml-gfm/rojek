#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
from pathlib import Path
import shutil  
import glob
from platform import python_version

METRIC = "k"

def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def createNpArray(tspath):
    vel_filename = "%s/%s" % (tspath, METRIC)
    #print(vel_filename)
    scene = []
    with open(vel_filename, 'r') as reader:
        headerlines = 21 
        head = [next(reader) for x in range(headerlines)]
        ncells = int(reader.readline().strip())
        reader.readline()
        #print(ncells)
        for lineid in range(ncells):
            line = reader.readline()
            lst = list(line[1:-2].split(" "))
            try:
                lstf = list(map(float, lst))
            except Exception as e:
                #print(lst)
                lstf = []
                for elem in lst:
                    aux = elem
                    if elem == 'e-0' or elem == '.' or elem == '':
                        aux = 0.0
                    lstf.append(aux)
            scene.append(lstf)    
    return np.array(scene, dtype = np.float16)       
 
if __name__ == "__main__":
    dirnpz = "NPZs%s" % METRIC
    Path(dirnpz).mkdir(parents=True, exist_ok=True)

    # Get timesteps
    (dirpath, dirnames, filenames) = next(os.walk("CASES"))
    for case in dirnames:
        if os.path.exists(dirnpz + "/" + case + ".npz") or os.path.exists(dirnpz + "/" + case + ".run"):
            continue
            
        print(case)
        command = "touch %s/%s.run" % (dirnpz, case)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
    
        (dirpath2, dirnames2, filenames2) = next(os.walk(dirpath + "/" + case))
        if len(dirnames2) < 420:
            print(case, "Not completed")
            
            command = "rm %s/%s.run" % (dirnpz, case)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
        
            continue
        ids=[]                
        for ts in dirnames2:
            if (is_number(ts)):
                if (int(ts) >= 0):
                    ids.append(int(ts))
        ids = sorted(ids)[2:]
        #print(len(ids))
        ds = [] 
        for ts in ids:   
            tspath = "%s/%d" % (dirpath2, ts)        
            ds.append(createNpArray(tspath))
        ds = np.array(ds) 
        print(ds.shape)
        filepath = "%s/%s" % (dirnpz, case)
        savez_compressed(filepath, data=ds, header=None)
        
        command = "rm %s/%s.run" % (dirnpz, case)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
